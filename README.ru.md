# testbox

>Утилита для интеграционного тестирования http еднпоинтов.

- [testbox](#testbox)
  - [Назначение компонента](#назначение-компонента)
  - [Перед первым началом работы с исходным кодом компонента](#перед-первым-началом-работы-с-исходным-кодом-компонента)
  - [Для построения компонента](#для-построения-компонента)
  - [Для получения результатов построения компонента](#для-получения-результатов-построения-компонента)
  - [Требования к окружению для работы компонента](#требования-к-окружению-для-работы-компонента)
  - [Описание параметров конфигурации компонента](#описание-параметров-конфигурации-компонента)
    - [Таблица параметров конфигурации](#таблица-параметров-конфигурации)
    - [Ключи запуска](#ключи-запуска)
    - [Переменные среды окружения](#переменные-среды-окружения)
    - [jsonSchema валидации](#jsonschema-валидации)
    - [Версионирование](#версионирование)
  - [Getting started](#getting-started)
  - [Add your files](#add-your-files)
  - [Integrate with your tools](#integrate-with-your-tools)
  - [Collaborate with your team](#collaborate-with-your-team)
  - [Test and Deploy](#test-and-deploy)
- [Editing this README](#editing-this-readme)
  - [Suggestions for a good README](#suggestions-for-a-good-readme)
  - [Name](#name)
  - [Description](#description)
  - [Badges](#badges)
  - [Visuals](#visuals)
  - [Installation](#installation)
  - [Usage](#usage)
  - [Support](#support)
  - [Roadmap](#roadmap)
  - [Contributing](#contributing)
  - [Authors and acknowledgment](#authors-and-acknowledgment)
  - [License](#license)
  - [Project status](#project-status)

## Назначение компонента

>v0.0.X - исключительно http endpoints

Утилита `testbox` предназначена для проведения интеграционного тестирования http endpoint-ов. Собирает docker образ тестируемого приложения, скачивает docker образы зависимостей и запускает docker контейнеры, объединяя в одну docker сеть и затем запуская тесты и формирует отчет.

![testenv](docs/testenv.c4.png)

## Перед первым началом работы с исходным кодом компонента

- Установить `go >=1.17`
- Установить `git`
- Клонировать репозиторий `git@gitlab.slurm.io:go_s010924/gotemplate.git`
- Перейти в папку с утилитой `cd ./Exercises/8.Docker/2`
- Можно запустить тесты `go test -cover -race ./...`

>Важно! Требуется иметь установленный docker.

Основные `internal` пакеты:

- `internal/config` - отвечает за чтение и валидацию конфигурационного файла приложения
- `internal/testsuit` - основная доменная область приложения, содержит все основные пакеты
  - `internal/testsuit/model` - модели данных доменной области
    - `internal/testsuit/model/checker/http` - http client - реализация основных функций проверок
  - `internal/testsuit/infra` - отвечает за создание, настройку и удаление окружения через работу с docker-api
  - `internal/testsuit/executor` - отвечает за конфигурирование и вызов необходимых операций тестирования
- `internal/process` - отвечает за запуск поднадзорного приложения, проксирование сигналов ОС, перехват stdout и stderr interceptor-ом
- `pkg/utils/randutils` - пакет с набором полезных функций генерации псевдослучайных наборов символов

## Для построения компонента

build приложения выполняется стандартно: `go build -o ./bin/testbox ./cmd/testbox` и в папке `bin` у вас должен появиться бинарный исполняемый файл `testbox`

## Для получения результатов построения компонента

Результатом построения (build-a) является бинарный исполняемый файл.
Переносите его в $PATH и можно пользоваться

**Запуск**: `testbox -f tests/e2e_testbox/config.yaml -v`

в `Makefile`-e есть инструкции по локальной сборке и запуску простых тестов:

```bash
make run                                                                                                                          (presto/default)
go build -o ./bin/testbox ./cmd/testbox
./bin/testbox -f configs/config4test.toml -v
[testID:KO0Y901] use config file configs/config4test.toml
[testID:KO0Y901] 2022-01-29T01:13:01+03:00: image built f86a6afcf194, name: container4test-ko0y901pv39u
[testID:KO0Y901] 2022-01-29T01:13:03+03:00: success pull image registry.gitlab.com/art.frela/simplestapi:v1.0.0
[testID:KO0Y901] 2022-01-29T01:13:03+03:00: network created 01e6690fbd061f3b158f6fa7553103867df9b38f1e187c61e7698f4f718a4ddd, name: container4test
[testID:KO0Y901] 2022-01-29T01:13:03+03:00: container created d8af3b27a81c6c7d12cdd70b6878993c513f9125ece7788f65e285e0936eecb1, name: someapi
[testID:KO0Y901] 2022-01-29T01:13:03+03:00: container runned d8af3b27a81c6c7d12cdd70b6878993c513f9125ece7788f65e285e0936eecb1, name: someapi
[testID:KO0Y901] 2022-01-29T01:13:05+03:00: container created d732fe6203a59b6af258ab728289c1349151a205979d3047f963135099433760, name: container4test
[testID:KO0Y901] 2022-01-29T01:13:06+03:00: container runned d732fe6203a59b6af258ab728289c1349151a205979d3047f963135099433760, name: container4test
[testID:KO0Y901] 2022-01-29T01:13:11+03:00: run inverse for [run container] container4test
[testID:KO0Y901] 2022-01-29T01:13:16+03:00: run inverse for [create container] container4test
[testID:KO0Y901] 2022-01-29T01:13:22+03:00: run inverse for [run container] someapi
[testID:KO0Y901] 2022-01-29T01:13:27+03:00: run inverse for [create container] someapi
[testID:KO0Y901] 2022-01-29T01:13:32+03:00: run inverse for [create network] container4test
[testID:KO0Y901] 2022-01-29T01:13:37+03:00: run inverse for [create image] container4test-ko0y901pv39u
# report
cat report.txt                                                                                                                    (presto/default)
report: Test suite KO0Y901 run at: 2022-01-29T01:13:11+03:00
simple query    OK
simple query 2  FAIL (ExpectedResponseContains test got <!DO..)
simple POST     FAIL (ExpectedCode 204 got 404)
```

## Требования к окружению для работы компонента

На текущий момент `testbox` предназначен для работы в операционной системе `linux/MacOS`, для работы на других ОС не тестировался.  
Для работы нужен файл конфигурации, путь к которому указывается через флаг `-f`. Формат файла может быть в `json`, `yaml/yml`, `toml` форматах.
Целевой `Dockerfile` должен лежать в корне тестируемого приложения (пока не будет добавлен параметр `context`)

## Описание параметров конфигурации компонента

Файл конфигурации после прочтения проходит валидацию на соответствие jsonSchema-е см ниже.  
Префикс для состава имен переменных окружения `TESTBOX_`. Значения в переменные окружения перезаписывают значения в конфиг файле.

```yaml
containerName: mycontainer    # наименование docker контейнера при тестировании
environment:                  # массив переменных окружения, передаваемых в docker контейнер
  - DEBUG=1
  - DB=${db:network_address}  # формат записи ${db:network_address} означае получение параметра из зависимого docker контейнера с именем db, параметр network_address
dependencies:                 # массив зависимостей в виде параметров для запуска docker контейнеров
  - name: db                  # наименование docker контейнера зависимости
    environment:              # массив переменных окружения, передаваемых в docker контейнер зависимости
      - DEBUG=1
    image: imagename          # имя docker образа, для запуска зависимости
dockerfile: ./Dockerfile      # имя dockerfile-a
ports:                        # порты, которые нужно открыть для тестируемого приложения
  - 80/tcp:8080
tests:                        # массив тестов, которые необходимо выполнить над тестируемым приложением
  - responseContains: test    # текст, который должен содержаться в ответе на запрос
    expectedCode: 200         # ожидаемый http код ответа
    name: simple query        # наименование теста
    queryType: GET            # тип http запроса: GET, POST, PUT, DELETE
    url: /list                # url path к тестируемому приложению
  - expectedCode: 200
    name: add info
    query: '{"insert":1}'     # содержимое тела POST/PUT запросов
    queryType: POST
    url: /add
```

### Таблица параметров конфигурации

| Параметр                   | Обязательный | Пояснение                                                                | Значение по умолчанию |
| -------------------------- | :----------: | ------------------------------------------------------------------------ | --------------------- |
| containerName              |    **да**    | наименование docker контейнера при тестировании                          |                       |
| environment                |    *нет*     | массив переменных окружения, передаваемых в docker контейнер             | пусто                 |
| dependencies               |    *нет*     | массив зависимостей в виде параметров для запуска docker контейнеров     | пусто                 |
| dependencies.0.name        |    **да**    | наименование docker контейнера зависимости                               |                       |
| dependencies.0.environment |    *нет*     | массив переменных окружения, передаваемых в docker контейнер зависимости | пусто                 |
| dependencies.0.image       |    **да**    | имя docker образа, для запуска зависимости                               |                       |
| dockerfile                 |    **да**    | имя dockerfile-a                                                         |                       |
| ports                      |    **да**    | порты, которые нужно открыть для тестируемого приложения                 |                       |
| tests                      |    **да**    | массив тестов, которые необходимо выполнить над тестируемым приложением  |                       |
| tests.0.responseContains   |    *нет*     | текст, который должен содержаться в ответе на запрос                     |                       |
| tests.0.expectedCode       |    *нет*     | ожидаемый http код ответа, если не указан, то не проверяется             |                       |
| tests.0.name               |    **да**    | наименование теста                                                       |                       |
| tests.0.queryType          |    **да**    | тип http запроса: GET, POST, PUT, DELETE                                 |                       |
| tests.0.query              |    *нет*     | содержимое тела POST/PUT запросов (application/json)                     | пусто                 |
| tests.0.url                |    **да**    | url path к тестируемому приложению                                       |                       |

### Ключи запуска

```bash
./bin/testbox -h`
#>
Usage of ./bin/testbox:
  -c, --clear           Teardown infra (default true)
  -f, --file string     Path to testsuit file (env: TESTBOX_CONFIG_PATH) (default "config.toml")
  -o, --output string   Output for test report (default "report.txt")
  -v, --verbose         Prints events
pflag: help requested
```

| Ключ          | Описание                                                                              |
| ------------- | ------------------------------------------------------------------------------------- |
| -c, --clear   | Флаг, удалять или нет созданные и запущенные docker контейнеры, образы и сети         |
| -v, --verbose | Печать в stdout событий создания и удаления docker сущностей                          |
| -f, --file    | Путь к файлу конфигурации теста  (env: `TESTBOX_CONFIG_PATH`) (default `config.toml`) |
| -o, --output  | Путь к файлу отчета, по умоляанию `report.txt`                                        |
| -h, --help    | Выводит в консоль справку по использованию и завершается                              |

### Переменные среды окружения

Имя переменной среды формируется из двух частей, префикса `TESTBOX` и имени переменной в верхнем регистре. Если переменная вложена, то это обозначается символом _. Переменные среды имеют высший приоритет.

Пример для `containerName` -> `TESTBOX_CONTAINERNAME`

### jsonSchema валидации

<details>
    <summary>config jsonSchema</summary>
    <code>
{
    "$schema": "http://json-schema.org/draft-07/schema",
    "$id": "https://gitlab.slurm.io/go_s010924/gotemplate/-/raw/main/Exercises/8.Docker/2/configs/testsuit_schema.json",
    "type": "object",
    "title": "The test suite schema",
    "description": "The root test suite config.",
    "required": [
        "containerName",
        "dockerfile",
        "ports",
        "tests"
    ],
    "properties": {
        "containerName": {
            "type": "string",
            "description": "The docker container name.",
            "pattern": "[a-zA-Z0-9][a-zA-Z0-9_.-]"
        },
        "environment": {
            "$ref": "#/definitions/environment"
        },
        "dependencies": {
            "type": "array",
            "items": {
                "type": "object",
                "title": "The dependencies schema",
                "description": "Dependencies which need to run before testing.",
                "required": [
                    "name",
                    "image"
                ],
                "properties": {
                    "name": {
                        "type": "string",
                        "description": "The dependency name."
                    },
                    "environment": {
                        "$ref": "#/definitions/environment"
                    },
                    "image": {
                        "type": "string",
                        "description": "Dependency docker image name.",
                        "pattern": "[a-zA-Z0-9][a-zA-Z0-9_.-]"
                    }
                }
            }
        },
        "dockerfile": {
            "type": "string",
            "description": "Path to the docker file.",
            "default": "./Dockerfile",
            "minLength": 1
        },
        "ports": {
            "type": "array",
            "items": {
                "type": "string",
                "description": "The nat port mapping.",
                "pattern": "[0-9]{2,4}/[tcud]{2}p:[0-9]{2,4}"
            },
            "minItems": 1
        },
        "tests": {
            "type": "array",
            "description": "Test parameters.",
            "items": {
                "required": [
                    "name",
                    "url",
                    "queryType"
                ],
                "properties": {
                    "name": {
                        "type": "string",
                        "description": "The test name.",
                        "minLength": 1
                    },
                    "url": {
                        "type": "string",
                        "description": "The endpoints for testing",
                        "default": "/",
                        "minLength": 1
                    },
                    "queryType": {
                        "type": "string",
                        "description": "The http methods",
                        "enum": [
                            "POST",
                            "GET",
                            "PUT",
                            "DELETE"
                        ]
                    },
                    "query": {
                        "type": "string",
                        "description": "POST/PUT request payload.",
                        "minLength": 1
                    },
                    "expectedCode": {
                        "type": "integer",
                        "description": "The HTTP code of the response.",
                        "minimum": 100,
                        "maximum": 505
                    },
                    "responseContains": {
                        "type": "string",
                        "description": "Response body should contain this test",
                        "minLength": 1
                    }
                }
            }
        }
    },
    "definitions": {
        "duration": {
            "type": "string",
            "pattern": "^([0-9]+[mnsuµh]*)+$"
        },
        "environment": {
            "type": "array",
            "title": "The docker container environment variables schema",
            "description": "Array of the environment variables.",
            "items": {
                "type": "string",
                "pattern": "^[a-zA-Z0-9]{1}[a-zA-Z0-9_]{0,63}=."
            }
        }
    }
}
</code>
</details>

### Версионирование

Осуществляется согласно [semver](https://semver.org/lang/ru/).


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/art.frela/testbox.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/art.frela/testbox/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
