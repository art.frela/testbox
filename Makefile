# h - help
h help:
	@echo "h help 	- this help"
	@echo "build 	- builds binary file to ./bin folder"
	@echo "run 	- build and run testbox with test config with 1 pass and 2 fail tests"
.PHONY: h

build:
	go build -o ./bin/testbox ./cmd/testbox
.PHONY: build

run: build
	./bin/testbox -f configs/config4test.toml
.PHONY: run
