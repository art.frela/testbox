package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"os"
	"time"

	"gitlab.com/art.frela/testbox/internal/config"
	"gitlab.com/art.frela/testbox/internal/testsuite/executor"
	"gitlab.com/art.frela/testbox/internal/testsuite/infra"
	"gitlab.com/art.frela/testbox/internal/testsuite/model"
	"gitlab.com/art.frela/testbox/pkg/utils/randutils"

	flag "github.com/spf13/pflag"
)

const (
	envPREFIX        = "TESTBOX"
	pathENV          = "TESTBOX_CONFIG_PATH"
	defOutput        = "report.txt"
	lenRandTestID    = 7
	pauseWaitInfraUP = time.Second * 5
)

func main() {
	verbose := flag.BoolP("verbose", "v", false, "Prints events")
	clear := flag.BoolP("clear", "c", true, "Teardown infra")
	pathToConfig := flag.StringP("file", "f", "config.toml", "Path to testsuit file (env: "+pathENV+")")
	outputStr := flag.StringP("output", "o", defOutput, "Output for test report")
	flag.Parse()

	var (
		testID = randutils.RandStringRunes(lenRandTestID)
		logger = log.New(os.Stdout, "[testID:"+testID+"] ", 0)
	)
	logger.Printf("use config file %s", *pathToConfig)

	// setup
	out, testsuite, dc, err := setup(*outputStr, *pathToConfig)
	if err != nil {
		logger.Printf("setup error %s", err)
		os.Exit(1)
	}

	// prepare infra
	infraItems, err := dc.Prepare(testsuite)
	defer teardown(dc, infraItems, *verbose, *clear, logger)

	if err != nil {
		logger.Printf("infra prepare error %s", err)
		return
	}
	time.Sleep(pauseWaitInfraUP) // pause for run containers.

	// infra ok can run tests
	exec := executor.New()
	tests, err := exec.BuildTests(dc.HostIP(), testsuite)
	if err != nil {
		logger.Printf("build tests error %s", err)
		return
	}
	results := exec.TestExec(tests)

	report := model.Report{
		ID:      testID,
		Date:    time.Now(),
		Results: results,
	}
	// print results
	fmt.Fprintf(out, "report: %s", report)
}

func teardown(dc *infra.DockerClient, infraItems infra.Items, verbose, clear bool, log *log.Logger) {
	ctx, cancel := context.WithTimeout(context.Background(), infra.TearDownTimeout)
	defer cancel()
	if clear {
		if er := dc.TearDown(ctx, infraItems); er != nil {
			log.Printf("infra.TearDown error: %s", er)
		}
	}
	//
	if verbose {
		for _, ev := range dc.Events() {
			log.Printf("%s", ev)
		}
	}
}

func setup(output, pathToConfig string) (io.WriteCloser, *config.CFG, *infra.DockerClient, error) {
	out, err := setupReportOutput(output)
	if err != nil {
		return nil, nil, nil, fmt.Errorf("setup report output error %w", err)
	}
	testsuite, err := setupTestsuite(pathToConfig)
	if err != nil {
		return nil, nil, nil, fmt.Errorf("config testsuite error %w", err)
	}
	dc, err := infra.NewDockerClient()
	if err != nil {
		return nil, nil, nil, fmt.Errorf("infra.NewDockerClient error %w", err)
	}
	return out, testsuite, dc, nil
}

func setupTestsuite(pathToConfig string) (*config.CFG, error) {
	// read config file
	configPath := os.Getenv(pathENV)
	if configPath != "" {
		pathToConfig = configPath
	}
	//
	testsuit, err := config.New(envPREFIX, config.Schema)
	if err != nil {
		return nil, fmt.Errorf("read config error %w", err)
	}

	if err := testsuit.Read(pathToConfig); err != nil {
		return nil, fmt.Errorf("read config error %w", err)
	}

	if err := testsuit.Validate(); err != nil {
		return nil, fmt.Errorf("invalid config %w", err)
	}
	return testsuit, nil
}

func setupReportOutput(output string) (io.WriteCloser, error) {
	var out io.WriteCloser
	if output != "stdout" && output != "" {
		f, err := os.Create(output)
		if err != nil {
			return nil, err
		}
		out = f
	}
	if output == "stdout" {
		out = os.Stdout
	}
	return out, nil
}
