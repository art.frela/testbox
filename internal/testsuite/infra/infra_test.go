package infra

import (
	"context"
	"testing"
	"time"

	"gitlab.com/art.frela/testbox/internal/config"
)

func TestDockerClient_buildImage(t *testing.T) {

	tests := []struct {
		name                string
		ctx                 context.Context
		docerRegistryUserID string
		dockerFile          string
		pathToDockerFile    string
		wantErr             bool
	}{
		{"nginx4test", context.Background(), "testbox/image:1.0.0", "build/Dockerfile", "./testdata/", false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dc, err := NewDockerClient()
			if err != nil {
				t.Errorf("NewDockerClient unexpected error %s", err)
				return
			}
			item, err := dc.imageBuild(tt.ctx, tt.docerRegistryUserID, tt.dockerFile, tt.pathToDockerFile)
			if (err != nil) != tt.wantErr {
				t.Errorf("DockerClient.buildImage() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			t.Logf("item: %+v", item)
			// err = item.Inverse(tt.ctx, item.ID)
			// if err != nil {
			// 	t.Errorf("item.Inverse unexpected error")
			// }
			// if !reflect.DeepEqual(got, dc.deleteImage) {
			// 	t.Errorf("DockerClient.buildImage() = %v, want %v", got, dc.deleteImage)
			// }
		})
	}
}

func TestDockerClient_pullImages(t *testing.T) {
	const (
		yamlValid = "./testdata/yaml/config.yaml"
	)

	tests := []struct {
		name    string
		pathCfg string
		wantErr bool
	}{
		{"test with images", yamlValid, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c, err := config.New("TEST", config.Schema)
			if err != nil {
				t.Errorf("config.New() unexpected error %s", err)
				return
			}
			err = c.Read(tt.pathCfg)
			if err != nil {
				t.Errorf("config.Read() unexpected error %s", err)
				return
			}
			dc, err := NewDockerClient()
			if err != nil {
				t.Errorf("NewDockerClient() unexpected error %s", err)
				return
			}
			if err := dc.pullImages(context.Background(), c); (err != nil) != tt.wantErr {
				t.Errorf("DockerClient.pullImages() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestDockerClient_createNetwork(t *testing.T) {
	tests := []struct {
		name        string
		networkName string
		wantErr     bool
	}{
		{"network example1", "example1", false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dc, err := NewDockerClient()
			if err != nil {
				t.Errorf("NewDockerClient() error = %v", err)
				return
			}
			got, err := dc.networkCreate(context.Background(), tt.networkName)
			if (err != nil) != tt.wantErr {
				t.Errorf("DockerClient.createNetwork() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			t.Logf("network: %+v", got)
			if err := got.Inverse(context.Background(), got.ID); err != nil {
				t.Errorf("Inverse() error = %v", err)
			}
		})
	}
}

func TestDockerClient_Prepare(t *testing.T) {
	tests := []struct {
		name    string
		cfg     string
		wantErr bool
	}{
		{"test config4test", "./config4test.toml", false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c, err := config.New("TEST", config.Schema)
			if err != nil {
				t.Errorf("config.New() unexpected error %s", err)
				return
			}
			err = c.Read(tt.cfg)
			if err != nil {
				t.Errorf("config.Read() unexpected error %s", err)
				return
			}
			dc, err := NewDockerClient()
			if err != nil {
				t.Errorf("NewDockerClient() error = %v", err)
				return
			}
			got, err := dc.Prepare(c)
			if (err != nil) != tt.wantErr {
				t.Errorf("DockerClient.Prepare() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			defer func() {
				// if err := dc.TearDown(context.Background(), got); err != nil {
				// 	t.Errorf("TearDown() error = %v", err)
				// }
				for _, ev := range dc.events {
					t.Logf("%s\n", ev)
				}
			}()
			t.Logf("items: %+v", got)
			time.Sleep(timeoutStopContainer)
		})
	}
}
