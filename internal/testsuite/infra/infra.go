// Package infra contains docker client wrapper for creates,
// runs and stops with dropping container entities (images, networks, containers and etc...).
package infra

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"strings"
	"time"

	"gitlab.com/art.frela/testbox/internal/config"
	"gitlab.com/art.frela/testbox/pkg/utils/randutils"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/network"
	"github.com/docker/docker/client"
	"github.com/docker/docker/pkg/archive"
	"github.com/docker/go-connections/nat"
)

const (
	lenRandImageSuffix = 12
	pathToDockerFile   = "."
	timeoutBuildImage  = time.Second * 60 * 10
	pauseRunContainer  = time.Millisecond * 500 * 3
	TearDownTimeout    = time.Second * 30
)

var (
	ErrEmptyDockerfile    = errors.New("empty docker file")
	ErrEmptyContainerName = errors.New("empty containerName not allowed")
	//
	timeoutStopContainer = time.Second * 5
)

// DockerClient wrapper around docker sdk client.
type DockerClient struct {
	c      *client.Client
	events []string
}

// NewDockerClient builder for DockerClient.
func NewDockerClient() (*DockerClient, error) {
	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		return nil, err
	}
	c := &DockerClient{c: cli, events: make([]string, 0)}
	return c, nil
}

// Prepare builds docker image, pull dependencies images,
// runs dependencies and runs target docker container.
func (dc *DockerClient) Prepare(testsuit *config.CFG) (Items, error) {
	ctx, cancel := context.WithTimeout(context.Background(), timeoutBuildImage)
	defer cancel()
	// imageItem
	imageItem, err := dc.prepareImage(ctx, testsuit)
	if err != nil {
		return nil, fmt.Errorf("prepare docker image %w", err)
	}
	items := Items{*imageItem}
	// pull dependencies image
	err = dc.pullImages(ctx, testsuit)
	if err != nil {
		return items, fmt.Errorf("pull docker images %w", err)
	}
	containerName := testsuit.GetString("containerName")
	if containerName == "" {
		return items, fmt.Errorf("empty containerName not allowed")
	}
	// run dependencies containers
	networkItem, err := dc.networkCreate(ctx, containerName)
	if err != nil {
		return items, fmt.Errorf("create docker network %w", err)
	}
	items.add(*networkItem)
	//
	runnedItems, err := dc.runContainerWithDependencies(ctx, networkItem.Name, imageItem.Name, testsuit)
	items = append(items, runnedItems...)
	//
	return items, err
}

// TearDown get docker entity from stack and call inverse operation,
// for "image build" invers will be "image rm".
func (dc *DockerClient) TearDown(ctx context.Context, ii Items) (err error) {
	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf("recovered from %v", r)
		}
	}()
	errMessage := make([]string, 0)
	for item := ii.get(); item != nil; item = ii.get() {
		dc.addEvent("run inverse for [" + string(item.Kind) + "] " + item.Name)
		err := item.Inverse(ctx, item.ID)
		if err != nil {
			errMessage = append(errMessage, err.Error())
		}
		time.Sleep(timeoutStopContainer)
	}
	if len(errMessage) > 0 {
		return fmt.Errorf("error happen %v", errMessage)
	}
	return nil
}

// HostIP extracts docker host ip.
func (dc *DockerClient) HostIP() string {
	ip := "0.0.0.0"
	if dh := dc.c.DaemonHost(); !strings.HasPrefix(dh, "unix://") {
		ip = dh
	}
	return ip
}

// Events extracts events.
func (dc *DockerClient) Events() []string {
	return dc.events
}

// containerItem is local minimalist container model.
type containerItem struct {
	name        string
	image       string
	networkName string
	envs        []string
	extPorts    map[string]nat.Port
}

func (dc *DockerClient) runContainerWithDependencies(ctx context.Context,
	networkName, mainImage string, testsuit *config.CFG) (Items, error) {
	//
	mainContainer, err := dc.makeMainContainerItem(mainImage, networkName, testsuit)
	if err != nil {
		return nil, err
	}

	// run dependencies
	deps := testsuit.GetSliceMap("dependencies")
	containers, err := dc.makeDependContainerItems(networkName, deps)
	if err != nil {
		return nil, err
	}
	items := make(Items, 0, len(containers))
	for _, c := range containers {
		cItems, er := dc.containerCreateAndRun(ctx, c)
		items = append(items, cItems...)
		if er != nil {
			return items, er
		}
		time.Sleep(pauseRunContainer)
	}

	// get env parameters for extract from dependencies
	mainEnvs, err := dc.makeEnvs(ctx, testsuit.GetSliceStringAsIs("environment"), networkName, items)
	if err != nil {
		return items, err
	}

	// run main container
	// TODO: add healthchecks.
	mainContainer.envs = mainEnvs
	cItems, err := dc.containerCreateAndRun(ctx, mainContainer)
	items = append(items, cItems...)
	return items, err
}

//nolint:gocognit
func (dc *DockerClient) makeDependContainerItems(networkName string,
	deps []map[string]interface{}) ([]containerItem, error) {
	//
	containers := make([]containerItem, 0, len(deps))
	for i, dep := range deps {
		namei, ok := dep["name"]
		if !ok {
			return nil, fmt.Errorf("dependency #%d without name", i)
		}
		name, assertOK := namei.(string)
		if !assertOK {
			return nil, fmt.Errorf("dependency #%d name %v/(type: %T) isn't string", i, name, name)
		}

		imgi, imgok := dep["image"]
		if !imgok {
			return nil, fmt.Errorf("dependency #%d without image", i)
		}
		img, assertOK := imgi.(string)
		if !assertOK {
			return nil, fmt.Errorf("dependency #%d image %v/(type: %T) isn't string", i, img, img)
		}
		c := containerItem{
			name:        name,
			image:       img,
			networkName: networkName,
		}
		if envsi, ok := dep["environment"]; ok {
			if envssli, assertOK := envsi.([]interface{}); assertOK {
				envs := make([]string, 0, len(envssli))
				for _, ei := range envssli {
					if e, strOK := ei.(string); strOK {
						envs = append(envs, e)
					}
				}
				c.envs = envs
			}
		}
		containers = append(containers, c)
	}
	return containers, nil
}

func (dc *DockerClient) makeMainContainerItem(mainImage, networkName string,
	testsuit *config.CFG) (containerItem, error) {
	//
	const lenParts = 2
	mainContainerName := testsuit.GetString("containerName")
	if mainContainerName == "" {
		return containerItem{}, fmt.Errorf("empty containerName not allowed")
	}
	ports := testsuit.GetSliceStringAsIs("ports")
	if len(ports) == 0 {
		return containerItem{}, fmt.Errorf("empty ports not allowed")
	}
	extPorts := map[string]nat.Port{}
	for _, portMap := range ports {
		portParts := strings.Split(portMap, ":")
		if len(portParts) == lenParts {
			extPorts[portParts[1]] = nat.Port(portParts[0])
		}
	}
	mainContainer := containerItem{
		name:        mainContainerName,
		image:       mainImage,
		networkName: networkName,
		extPorts:    extPorts,
	}
	return mainContainer, nil
}

func (dc *DockerClient) makeEnvs(ctx context.Context,
	srcEnvs []string, networkName string, items Items) ([]string, error) {
	//
	const lenParts = 2
	mainEnvs := make([]string, 0)
	for _, e := range srcEnvs {
		if ix := strings.Index(e, "${"); ix >= 0 {
			eValue := strings.TrimLeft(strings.TrimRight(e[ix:], "}"), "${")
			parts := strings.Split(eValue, ":")
			if len(parts) == lenParts {
				rContainer := items.searchByName(IKContainerRun, parts[0])
				if rContainer == nil {
					continue
				}
				value, err := dc.getAttribute(ctx, rContainer.ID, parts[1], networkName)
				if err != nil {
					return nil, err
				}
				eParts := strings.Split(e, "=")
				if len(eParts) == lenParts {
					envItem := fmt.Sprintf("%s=%s", eParts[0], value)
					mainEnvs = append(mainEnvs, envItem)
				}
			}
			continue
		}
		mainEnvs = append(mainEnvs, e)
	}
	return mainEnvs, nil
}

func (dc *DockerClient) getAttribute(ctx context.Context, cid, attrName, networkName string) (string, error) {
	info, err := dc.c.ContainerInspect(ctx, cid)
	if err != nil {
		return "", err
	}
	switch attrName {
	case "network_address": // TODO: replace to constant and add some parameter mapping.
		if info.NetworkSettings != nil {
			if net, ok := info.NetworkSettings.Networks[networkName]; ok {
				if net != nil {
					return net.IPAddress, nil
				}
			}
			return info.NetworkSettings.IPAddress, nil
		}
		return "", nil
	default:
		return "", nil
	}
}

func (dc *DockerClient) containerCreateAndRun(ctx context.Context, cont containerItem) (Items, error) {
	items := make(Items, 0)
	itemC, err := dc.containerCreate(ctx, cont)
	if err != nil {
		return nil, err
	}
	items.add(*itemC)

	if er := dc.c.ContainerStart(ctx, itemC.ID, types.ContainerStartOptions{}); er != nil {
		return items, fmt.Errorf("start container %s error %w", cont.name, er)
	}
	itemR := Item{
		ID:      itemC.ID,
		Kind:    IKContainerRun,
		Name:    cont.name,
		Inverse: dc.containerStop,
	}
	dc.addEvent("container runned " + itemR.ID + ", name: " + itemR.Name)
	items.add(itemR)

	return items, nil
}

func (dc *DockerClient) containerCreate(ctx context.Context, cont containerItem) (*Item, error) {
	ports := nat.PortSet{}
	portBinding := nat.PortMap{}
	for extPort, port := range cont.extPorts {
		ports[port] = struct{}{}
		portBinding[port] = []nat.PortBinding{
			{
				HostIP:   "0.0.0.0",
				HostPort: extPort,
			},
		}
	}

	resp, err := dc.c.ContainerCreate(
		ctx,
		&container.Config{
			Image:        cont.image,
			Env:          cont.envs,
			ExposedPorts: ports,
		},
		&container.HostConfig{
			PortBindings: portBinding,
		},
		&network.NetworkingConfig{
			EndpointsConfig: map[string]*network.EndpointSettings{
				cont.networkName: {},
			},
		}, nil, cont.name)
	if err != nil {
		return nil, fmt.Errorf("container create %s error %w", cont.name, err)
	}
	item := &Item{
		ID:      resp.ID,
		Kind:    IKContainerCreate,
		Name:    cont.name,
		Inverse: dc.containerDelete,
	}
	dc.addEvent("container created " + item.ID + ", name: " + item.Name)
	return item, nil
}

// containerDelete deletes docker container.
func (dc *DockerClient) containerDelete(ctx context.Context, cid string) error {
	return dc.c.ContainerRemove(ctx, cid, types.ContainerRemoveOptions{RemoveVolumes: true, Force: true})
}

// containerStop stops docker container.
func (dc *DockerClient) containerStop(ctx context.Context, cid string) error {
	return dc.c.ContainerStop(ctx, cid, &timeoutStopContainer)
}

// networkCreate creates docker network.
func (dc *DockerClient) networkCreate(ctx context.Context, name string) (*Item, error) {
	resp, err := dc.c.NetworkCreate(ctx, name, types.NetworkCreate{})
	if err != nil {
		return nil, err
	}
	item := &Item{
		ID:      resp.ID,
		Kind:    IKNetworkCreate,
		Name:    name,
		Inverse: dc.networkDelete,
	}
	dc.addEvent("network created " + item.ID + ", name: " + item.Name)
	return item, nil
}

// networkDelete deletes docker network.
func (dc *DockerClient) networkDelete(ctx context.Context, nid string) error {
	return dc.c.NetworkRemove(ctx, nid)
}

func (dc *DockerClient) pullImages(ctx context.Context, testsuit *config.CFG) error {
	dependencies := testsuit.GetSliceMap("dependencies")
	for _, dep := range dependencies {
		if imgi, ok := dep["image"]; ok {
			if img, assertOK := imgi.(string); assertOK {
				pullResponse, err := dc.c.ImagePull(ctx, img, types.ImagePullOptions{})
				if err != nil {
					return err
				}
				buf := &bytes.Buffer{}
				if _, err := io.Copy(buf, pullResponse); err != nil {
					return fmt.Errorf("read pull image response error %w", err)
				}
				dc.addEvent("success pull image " + img)
				pullResponse.Close()
			}
		}
	}
	return nil
}

// prepareImage prepares image name, path to dockerfile.
func (dc *DockerClient) prepareImage(ctx context.Context, testsuit *config.CFG) (*Item, error) {
	// build docker image
	imgName := strings.ToLower(testsuit.GetString("containerName"))
	if imgName == "" {
		return nil, ErrEmptyContainerName
	}
	imgName += "-" + strings.ToLower(randutils.RandStringRunes(lenRandImageSuffix))
	dockerFile := testsuit.GetString("dockerfile")
	if dockerFile == "" {
		return nil, ErrEmptyDockerfile
	}
	return dc.imageBuild(ctx, imgName, dockerFile, pathToDockerFile)
}

// imageBuild creates docker image,
// src: https://www.loginradius.com/blog/async/build-push-docker-images-golang/
func (dc *DockerClient) imageBuild(ctx context.Context,
	dockerRegistryUserID, dockerFile, pathToDockerFile string) (*Item, error) {
	//
	tar, err := archive.TarWithOptions(pathToDockerFile, &archive.TarOptions{})
	if err != nil {
		return nil, err
	}
	opts := types.ImageBuildOptions{
		Dockerfile: dockerFile,
		Tags:       []string{dockerRegistryUserID},
		Remove:     true,
	}

	imageBuildResponse, err := dc.c.ImageBuild(ctx, tar, opts)
	if err != nil {
		return nil, err
	}
	imgID, err := dc.buildResponseProcessing(imageBuildResponse.Body)
	if err != nil {
		return nil, err
	}
	item := &Item{
		ID:      imgID,
		Kind:    IKImageCreate,
		Name:    dockerRegistryUserID,
		Inverse: dc.imageDelete,
	}
	dc.addEvent("image built " + item.ID + ", name: " + item.Name)
	return item, nil
}

//nolint:gocognit
func (dc *DockerClient) buildResponseProcessing(rc io.ReadCloser) (string, error) {
	const lenParts = 3
	defer rc.Close()
	scanner := bufio.NewScanner(rc)
	for scanner.Scan() {
		rawmsg := scanner.Bytes()
		var msg map[string]interface{}
		err := json.Unmarshal(rawmsg, &msg)
		if err != nil {
			return "", err
		}
		// {"stream":"Successfully built 4c44d0870e51\n"}
		if txti, ok := msg["stream"]; ok {
			if txt, asserOK := txti.(string); asserOK {
				if strings.Contains(strings.ToLower(txt), "successfully built") { // TODO: replace to regexp!?
					txt = strings.Trim(txt, "\n")
					parts := strings.Split(txt, " ")
					if len(parts) == lenParts {
						return parts[2], nil
					}
				}
			}
		}
		if erri, ok := msg["error"]; ok {
			if errStr, asserOK := erri.(string); asserOK {
				return "", fmt.Errorf("build error: %s", errStr)
			}
		}
	}
	return "", nil
}

func (dc *DockerClient) imageDelete(ctx context.Context, imgID string) error {
	_, err := dc.c.ImageRemove(ctx, imgID, types.ImageRemoveOptions{Force: true, PruneChildren: true})
	return err
}

func (dc *DockerClient) addEvent(msg string) {
	dc.events = append(dc.events, fmt.Sprintf("%s: %s", time.Now().Format(time.RFC3339), msg))
}
