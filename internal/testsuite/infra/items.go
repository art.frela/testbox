package infra

import "context"

const (
	IKImageCreate     ItemKind = "create image"
	IKContainerCreate ItemKind = "create container"
	IKContainerRun    ItemKind = "run container"
	IKNetworkCreate   ItemKind = "create network"
)

type InverseFunc func(ctx context.Context, id string) error

type Item struct {
	ID      string
	Kind    ItemKind
	Name    string
	Inverse InverseFunc
}

type ItemKind string

type Items []Item

func (ii *Items) get() *Item {
	if len(*ii) == 0 {
		return nil
	}
	out := (*ii)[len(*ii)-1]
	(*ii) = (*ii)[:len(*ii)-1]
	return &out
}

func (ii *Items) add(item Item) {
	*ii = append(*ii, item)
}

func (ii *Items) searchByName(kind ItemKind, name string) *Item {
	if ii == nil {
		return nil
	}
	for _, item := range *ii {
		if item.Kind == kind && item.Name == name {
			return &item
		}
	}
	return nil
}
