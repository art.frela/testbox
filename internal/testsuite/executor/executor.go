// Package executor contains oprations with use cases on tests.
package executor

import (
	"strings"

	"gitlab.com/art.frela/testbox/internal/config"
	"gitlab.com/art.frela/testbox/internal/testsuite/model"
	"gitlab.com/art.frela/testbox/internal/testsuite/model/checker/http"
)

const (
	defTargetPort = "8080"
	defTargetHost = "localhost"

	cfgKeyTests        = "tests"
	cfgKeyName         = "name"
	cfgKeyQueryType    = "queryType"
	cfgKeyPorts        = "ports"
	cfgKeyURL          = "url"
	cfgKeyQuery        = "query"
	cfgKeyExpectedCode = "expectedCode"
	cfgKeyRespContains = "responseContains"
)

// Executor mock for future using with private fields.
type Executor struct{}

// New builder for Executor.
func New() *Executor {
	return &Executor{}
}

// BuildTests creates tests from config testsuits.
func (e *Executor) BuildTests(targetHost string, testsuit *config.CFG) (model.Tests, error) {
	tests := testsuit.GetSliceMap(cfgKeyTests)
	if len(tests) == 0 {
		return nil, nil
	}
	if len(targetHost) == 0 {
		targetHost = defTargetHost
	}
	out := make(model.Tests, 0, len(tests))

	for _, testParams := range tests {
		test := makeTest(testParams, testsuit, targetHost)
		out = append(out, test)
	}
	return out, nil
}

// TestExec runs model.CheckFn for all tests.
func (e *Executor) TestExec(tests model.Tests) model.Results {
	results := make(model.Results, len(tests))
	for i, t := range tests {
		results[i] = t.Verify()
	}
	return results
}

// helpers

func makeTest(params map[string]interface{}, testsuit *config.CFG, targetHost string) model.Test {
	path, payload, responseContains, expCode := makeST(params)
	test := model.Test{
		Name:             params[cfgKeyName].(string), // TODO: check exists and assert.
		Code:             expCode,
		ResponseContains: responseContains,
	}

	u := makeURL(testsuit, targetHost)
	c, err := http.New(u, path, payload)
	if err != nil {
		chErr := model.NewCheckErr(err)
		test.Check = chErr.Check
		return test
	}

	var check model.CheckFn
	switch model.QueryType(strings.ToLower(params[cfgKeyQueryType].(string))) { // TODO: check exists and assert.
	case model.QTGET:
		check = c.GET
	case model.QTPOST:
		check = c.POST
	case model.QTPUT:
		check = c.PUT
	case model.QTDELETE:
		check = c.DELETE
	}
	test.Check = check
	return test
}

func makeURL(testsuit *config.CFG, targetHost string) string {
	const lenParts = 2
	port := defTargetPort
	ports := testsuit.GetSliceStringAsIs(cfgKeyPorts)
	if len(ports) > 0 {
		lastPort := ports[len(ports)-1]
		lastPortsParts := strings.Split(lastPort, ":")
		if len(lastPortsParts) == lenParts {
			port = lastPortsParts[1]
		}
	}
	return "http://" + targetHost + ":" + port
}

func makeST(params map[string]interface{}) (path, payload, respContains string, code int) {
	path = "/"
	if urli, ok := params[cfgKeyURL]; ok {
		if p, assertOK := urli.(string); assertOK {
			path = p
		}
	}
	payload = ""
	if qi, ok := params[cfgKeyQuery]; ok {
		if q, assertOK := qi.(string); assertOK {
			payload = q
		}
	}

	if codei, ok := params[cfgKeyExpectedCode]; ok {
		if codef64, assertOK := codei.(float64); assertOK {
			code = int(codef64)
		}
	}
	if rci, ok := params[cfgKeyRespContains]; ok {
		if rc, assertOK := rci.(string); assertOK {
			respContains = rc
		}
	}
	return path, payload, respContains, code
}
