package executor

import (
	"testing"

	"gitlab.com/art.frela/testbox/internal/config"
	"gitlab.com/art.frela/testbox/internal/testsuite/model"
	"gitlab.com/art.frela/testbox/internal/testsuite/model/checker/http"
)

func TestExecutor_BuildTests(t *testing.T) {
	const (
		yamlConfig = "testdata/yaml/config.yaml"
	)
	c, err := http.New("http://localhost:8080", "/list", "")
	if err != nil {
		t.Errorf("http.New() error %s", err)
		return
	}
	tests := []struct {
		name       string
		pathCfg    string
		targetHost string
		want       model.Tests
		wantErr    bool
	}{
		{
			"тест. Проверка 2ух тестов GET, POST",
			yamlConfig,
			"localhost",
			model.Tests{
				model.Test{
					Name:             "simple query",
					ResponseContains: "test",
					Code:             200,
					Check:            c.GET,
				},
				model.Test{
					Name:  "add info",
					Code:  200,
					Check: c.POST,
				},
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cfg, err := config.New("TEST", config.Schema)
			if err != nil {
				t.Errorf("config.New() unexpected error %s", err)
				return
			}
			err = cfg.Read(tt.pathCfg)
			if err != nil {
				t.Errorf("config.Read() unexpected error %s", err)
				return
			}
			e := New()
			got, err := e.BuildTests(tt.targetHost, cfg)
			if (err != nil) != tt.wantErr {
				t.Errorf("Executor.BuildTests() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !compareTests(got, tt.want) {
				t.Errorf("Executor.BuildTests() = %v, want %v", got, tt.want)
			}
		})
	}
}

func compareTests(t1, t2 model.Tests) bool {
	if len(t1) != len(t2) {
		return false
	}
	if len(t1) == 0 {
		return true
	}

	for _, t1item := range t1 {
		exists := false
		for _, t2item := range t2 {
			eqNames := t1item.Name == t2item.Name
			eqCodes := t1item.Code == t2item.Code
			eqResponse := t1item.ResponseContains == t2item.ResponseContains
			if eqNames && eqCodes && eqResponse {
				exists = true
			}
		}
		if !exists {
			return false
		}
	}
	return true
}
