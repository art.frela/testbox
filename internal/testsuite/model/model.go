// Package model contains domain layer business entity models.
package model

import (
	"fmt"
	"strings"
	"time"
)

const (
	QTGET    QueryType = "get"
	QTPOST   QueryType = "post"
	QTPUT    QueryType = "put"
	QTDELETE QueryType = "delete"
)

type (
	QueryType string
	CheckFn   func() Result
)

// CheckErr has a dummy CheckFn implementation.
type CheckErr struct {
	err error
}

// NewCheckErr builder for CheckErr.
func NewCheckErr(err error) *CheckErr {
	return &CheckErr{err: err}
}

// Check is CheckFn implementation.
func (ce *CheckErr) Check() Result {
	return Result{
		Err: ce.err,
	}
}

// Test test data model.
type Test struct {
	Name             string
	ResponseContains string
	Code             int
	Check            CheckFn
}

// Verify runs CheckFn and verify result by comparing with expected parameters.
func (t *Test) Verify() Result {
	if t.Check == nil {
		return Result{
			TestName: t.Name,
			Err:      fmt.Errorf("empty test check function"),
		}
	}
	res := t.Check()
	if res.Err != nil {
		res.TestName = t.Name
		return res
	}
	const maxErrLen = 2
	errMessage := make([]string, 0, maxErrLen)
	//
	if !strings.Contains(res.Response, t.ResponseContains) {
		shortResponse := res.Response
		if len(shortResponse) > len(t.ResponseContains) {
			shortResponse = res.Response[:len(t.ResponseContains)] + ".."
		}
		errMessage = append(errMessage, fmt.Sprintf("ExpectedResponseContains %s got %s", t.ResponseContains, shortResponse))
	}
	//
	if res.Code != t.Code && t.Code > 0 {
		errMessage = append(errMessage, fmt.Sprintf("ExpectedCode %d got %d", t.Code, res.Code))
	}
	out := Result{
		TestName: t.Name,
		Code:     res.Code,
		Response: res.Response,
	}
	if len(errMessage) > 0 {
		out.Err = fmt.Errorf("%s", strings.Join(errMessage, "; "))
	}
	return out
}

// Tests slice of the Test.
type Tests []Test

// Result model of the test results.
type Result struct {
	TestName string
	Code     int
	Response string
	Err      error
}

// String implements stringer.
func (r Result) String() string {
	res := "OK"
	if r.Err != nil {
		res = fmt.Sprintf("FAIL (%s)", r.Err)
	}
	return fmt.Sprintf("%s\t%s", r.TestName, res)
}

// Results slice of the Result.
type Results []Result

// Report model of the all tests results.
type Report struct {
	ID      string
	Date    time.Time
	Results Results
}

// String implements stringer.
func (r Report) String() string {
	out := "Test suite " + r.ID + " run at: " + r.Date.Format(time.RFC3339) + "\n"
	for _, res := range r.Results {
		out += res.String() + "\n"
	}
	return out
}
