// Package http containf http client implements model.CheckFn function.
package http

import (
	"crypto/tls"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"gitlab.com/art.frela/testbox/internal/testsuite/model"
)

const (
	maxIdleConns = 10
	timeout      = time.Millisecond * 1000 * 10
)

// Checker is a wrapper around http.Client.
type Checker struct {
	url     string
	path    string
	payload string
	c       *http.Client
}

// New builder for Checker.
func New(uri, path, payload string) (*Checker, error) {
	if _, err := url.Parse(uri); err != nil {
		return nil, err
	}
	// nolint: gosec
	cfg := &tls.Config{
		InsecureSkipVerify: true,
	}
	// set timeouts and border for opened connections
	handler := &http.Client{Timeout: timeout, Transport: &http.Transport{
		MaxIdleConns:    maxIdleConns,
		IdleConnTimeout: timeout,
		TLSClientConfig: cfg,
	}}

	return &Checker{
		url:     uri,
		path:    path,
		payload: payload,
		c:       handler,
	}, nil
}

// GET does get http request.
func (c *Checker) GET() model.Result {
	u := c.url + "/" + strings.TrimLeft(c.path, "/")
	return c.request(http.MethodGet, u, nil, nil)
}

// GET does post http request.
func (c *Checker) POST() model.Result {
	u := c.url + "/" + strings.TrimLeft(c.path, "/")
	payload := strings.NewReader(c.payload)
	headers := [][2]string{{"Content-type", "application/json"}}
	return c.request(http.MethodPost, u, payload, headers)
}

// PUT does put http request.
func (c *Checker) PUT() model.Result {
	u := c.url + "/" + strings.TrimLeft(c.path, "/")
	payload := strings.NewReader(c.payload)
	headers := [][2]string{{"Content-type", "application/json"}}
	return c.request(http.MethodPut, u, payload, headers)
}

// DELETE does delete http request.
func (c *Checker) DELETE() model.Result {
	u := c.url + "/" + strings.TrimLeft(c.path, "/")
	return c.request(http.MethodDelete, u, nil, nil)
}

// request common http requester.
func (c *Checker) request(method, uri string, payload io.Reader, headers [][2]string) model.Result {
	res := model.Result{}
	req, err := http.NewRequest(method, uri, payload)
	if err != nil {
		res.Err = fmt.Errorf("new http request error %w", err)
		return res
	}
	for _, header := range headers {
		req.Header.Add(header[0], header[1])
	}

	resp, err := c.c.Do(req)
	if err != nil {
		res.Err = fmt.Errorf("http do request error %w", err)
		return res
	}
	defer resp.Body.Close()
	res.Code = resp.StatusCode
	bodyBts, err := io.ReadAll(resp.Body)
	if err != nil {
		res.Err = fmt.Errorf("read response body error %w", err)
		return res
	}
	res.Response = string(bodyBts)
	return res
}
