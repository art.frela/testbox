package config

import (
	"testing"
)

func TestCFG_Validate(t *testing.T) {
	const (
		yamlValid         = "./testdata/yaml/config.yaml"
		yamlInValid       = "testdata/yaml/config_incorrect_expected_code.yaml"
		tomlValid         = "./testdata/toml/config.toml"
		tomlInValid       = "testdata/toml/config_incorrect_test_name_miss.toml"
		jsonValid         = "./testdata/json/config.json"
		jsonInValid       = "testdata/json/config_incorrect_containername_miss.json"
		tomlENV           = "testdata/toml/config_env.toml"
		yamlInValidPorts0 = "testdata/yaml/config_incorrect_ports_incorrect.yaml"
	)
	tests := []struct {
		name       string
		configPath string
		wantErr    bool
	}{
		{
			"тест. НЕКорректный файл в yaml формате неверный формат портов, ожидаем ошибка валидации",
			yamlInValidPorts0,
			false,
		},
		{
			"тест. Корректный файл в yaml формате, ожидаем все ок",
			yamlValid,
			false,
		},
		{
			"тест. Корректный файл в toml формате, ожидаем все ок",
			tomlValid,
			false,
		},
		{
			"тест. Корректный файл в json формате, ожидаем все ок",
			jsonValid,
			false,
		},
		{
			"тест. НЕКорректный файл в yaml формате, ожидаем ошибку валидации",
			yamlInValid,
			true,
		},
		{
			"тест. НЕКорректный файл в toml формате, ожидаем ошибку валидации",
			tomlInValid,
			true,
		},
		{
			"тест. НЕКорректный файл в json формате, ожидаем ошибку валидации",
			jsonInValid,
			true,
		},
		{
			"тест. НЕКорректный файл в toml формате - env кривое имя, ожидаем ошибку валидации",
			tomlENV,
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cfg, err := New("TEST", Schema)
			if err != nil {
				t.Errorf("New unexpected error, %s", err)
				return
			}
			err = cfg.Read(tt.configPath)
			if err != nil {
				t.Errorf("cfg.Read() unexpected error, %s", err)
				return
			}
			// validate
			if err := cfg.Validate(); (err != nil) != tt.wantErr {
				t.Errorf("CFG.Validate() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
