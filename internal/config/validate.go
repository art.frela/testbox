package config

import (
	"errors"
	"fmt"

	"github.com/xeipuuv/gojsonschema"
)

// ErrNoSchema no schema error.
var ErrNoSchema = errors.New("empty schema")

// Validate validates config with early passed jsonSchema.
func (c *CFG) Validate() error {
	return c.jsonValidateXEI()
}

func (c *CFG) jsonValidateXEI() error {
	if c.schema == nil {
		return ErrNoSchema
	}
	documentLoader := gojsonschema.NewGoLoader(c.cfg.Value())
	result, err := gojsonschema.Validate(c.schema, documentLoader)
	if err != nil {
		return fmt.Errorf("validate err, %w", err)
	}
	errSlice := result.Errors()
	errMsg := ""
	for i := range errSlice {
		errMsg += errSlice[i].String() + "; "
	}
	if len(errMsg) > 0 {
		return errors.New(errMsg)
	}
	return nil
}

const Schema = `{
    "$schema": "http://json-schema.org/draft-07/schema",
    "$id": "https://gitlab.slurm.io/go_s010924/gotemplate/-/raw/main/Exercises/8.Docker/2/configs/testsuit_schema.json",
    "type": "object",
    "title": "The test suite schema",
    "description": "The root test suite config.",
    "required": [
        "containerName",
        "dockerfile",
        "ports",
        "tests"
    ],
    "properties": {
        "containerName": {
            "type": "string",
            "description": "The docker container name.",
            "pattern": "[a-zA-Z0-9][a-zA-Z0-9_.-]"
        },
        "environment": {
            "$ref": "#/definitions/environment"
        },
        "dependencies": {
            "type": "array",
            "items": {
                "type": "object",
                "title": "The dependencies schema",
                "description": "Dependencies which need to run before testing.",
                "required": [
                    "name",
                    "image"
                ],
                "properties": {
                    "name": {
                        "type": "string",
                        "description": "The dependency name."
                    },
                    "environment": {
                        "$ref": "#/definitions/environment"
                    },
                    "image": {
                        "type": "string",
                        "description": "Dependency docker image name.",
                        "pattern": "[a-zA-Z0-9][a-zA-Z0-9_.-]"
                    }
                }
            }
        },
        "dockerfile": {
            "type": "string",
            "description": "Path to the docker file.",
            "default": "./Dockerfile",
            "minLength": 1
        },
        "ports": {
            "type": "array",
            "items": {
                "type": "string",
                "description": "The nat port mapping.",
                "pattern": "[0-9]{2,4}/[tcud]{2}p:[0-9]{2,4}"
            },
            "minItems": 1
        },
        "tests": {
            "type": "array",
            "description": "Test parameters.",
            "items": {
                "required": [
                    "name",
                    "url",
                    "queryType"
                ],
                "properties": {
                    "name": {
                        "type": "string",
                        "description": "The test name.",
                        "minLength": 1
                    },
                    "url": {
                        "type": "string",
                        "description": "The endpoints for testing",
                        "default": "/",
                        "minLength": 1
                    },
                    "queryType": {
                        "type": "string",
                        "description": "The http methods",
                        "enum": [
                            "POST",
                            "GET",
                            "PUT",
                            "DELETE"
                        ]
                    },
                    "query": {
                        "type": "string",
                        "description": "POST/PUT request payload.",
                        "minLength": 1
                    },
                    "expectedCode": {
                        "type": "integer",
                        "description": "The HTTP code of the response.",
                        "minimum": 100,
                        "maximum": 505
                    },
                    "responseContains": {
                        "type": "string",
                        "description": "Response body should contain this test",
                        "minLength": 1
                    }
                }
            }
        }
    },
    "definitions": {
        "duration": {
            "type": "string",
            "pattern": "^([0-9]+[mnsuµh]*)+$"
        },
        "environment": {
            "type": "array",
            "title": "The docker container environment variables schema",
            "description": "Array of the environment variables.",
            "items": {
                "type": "string",
                "pattern": "^[a-zA-Z0-9]{1}[a-zA-Z0-9_]{0,63}=."
            }
        }
    }
}`
