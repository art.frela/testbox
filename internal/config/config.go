package config

import (
	"fmt"
	"io"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	enc "gitlab.com/art.frela/testbox/internal/config/encoding"
	"gitlab.com/art.frela/testbox/internal/config/encoding/json"
	"gitlab.com/art.frela/testbox/internal/config/encoding/toml"
	"gitlab.com/art.frela/testbox/internal/config/encoding/yaml"

	"github.com/tidwall/gjson"
	"github.com/xeipuuv/gojsonschema"
)

const (
	FormatYAML        string = "yaml"
	FormatYML         string = "yml"
	FormatJSON        string = "json"
	FormatTOML        string = "toml"
	envSeparator      string = "_"
	envParseSeparator string = "."
	x64               int    = 64
)

var (
	encoderRegistry  = enc.NewEncoderRegistry()
	decoderRegistry  = enc.NewDecoderRegistry()
	supportedFormats = []string{FormatJSON, FormatTOML, FormatYAML, FormatYML}
)

// nolint:errcheck
func init() {
	{
		codec := yaml.Codec{}
		encoderRegistry.RegisterEncoder(FormatYAML, codec)
		encoderRegistry.RegisterEncoder(FormatYML, codec)
		decoderRegistry.RegisterDecoder(FormatYAML, codec)
		decoderRegistry.RegisterDecoder(FormatYML, codec)
	}
	{
		codec := json.Codec{}
		encoderRegistry.RegisterEncoder(FormatJSON, codec)
		decoderRegistry.RegisterDecoder(FormatJSON, codec)
	}
	{
		codec := toml.Codec{}
		encoderRegistry.RegisterEncoder(FormatTOML, codec)
		decoderRegistry.RegisterDecoder(FormatTOML, codec)
	}
}

type CFG struct {
	envPrefix string
	cfg       *gjson.Result
	schema    gojsonschema.JSONLoader
}

// New builder for CFG with jsonSchema, sets unexporter structure's fields,
// jsonSchema requirements:
//
// references only in $ref = "#/definitions/*" format.
func New(prefix, schema string) (*CFG, error) {
	schemaLoader := gojsonschema.NewStringLoader(schema)
	return &CFG{envPrefix: strings.ToUpper(prefix), schema: schemaLoader}, nil
}

//
// Read reads files by passed paths (rootConfig, appConfigs...),
// merge them, equal keys will be replaced without inner structure comparison
// and fills DotConf fields.
func (c *CFG) Read(rootConfig string) error {
	rm, err := readConfigFileToMAP(rootConfig)
	if err != nil {
		return fmt.Errorf("read root config error: %w", err)
	}
	//
	bts, err := encoderRegistry.Encode(FormatJSON, rm)
	if err != nil {
		return fmt.Errorf("encode config to JSON error: %w", err)
	}
	res := gjson.ParseBytes(bts)
	c.cfg = &res
	return nil
}

// Get extracts k-path value as common interface{}.
func (c *CFG) Get(k string) interface{} {
	envValue := c.env(k)
	if envValue == "" {
		return c.cfg.Get(k).Value()
	}
	return envValue
}

// GetString extracts k-path value,
// values ${val} or $val will be replaced according to the values current environment variables.
func (c *CFG) GetString(k string) string {
	envValue := c.env(k)
	if envValue == "" {
		return os.ExpandEnv(c.cfg.Get(k).String())
	}
	return os.ExpandEnv(envValue)
}

// GetBool extracts k-path value,
// values ${val} or $val will be replaced according to the values current environment variables.
func (c *CFG) GetBool(k string) bool {
	envValue := c.env(k)
	if envValue == "" {
		switch v := c.cfg.Get(k).Value().(type) {
		case string:
			//nolint:errcheck
			bv, _ := strconv.ParseBool(os.ExpandEnv(v))
			return bv
		default:
			return c.cfg.Get(k).Bool()
		}
	}
	//nolint:errcheck
	bv, _ := strconv.ParseBool(os.ExpandEnv(envValue))
	return bv
}

// GetDuration extracts k-path value as time.Duration,
// values ${val} or $val will be replaced according to the values current environment variables.
func (c *CFG) GetDuration(k string) time.Duration {
	envValue := c.env(k)
	if envValue == "" {
		envValue = c.cfg.Get(k).String()
	}
	//nolint:errcheck
	dur, _ := time.ParseDuration(os.ExpandEnv(envValue))
	return dur
}

// GetFloat64 extracts k-path value as float64,
// values ${val} or $val will be replaced according to the values current environment variables.
func (c *CFG) GetFloat64(k string) float64 {
	envValue := c.env(k)
	if envValue == "" {
		switch v := c.cfg.Get(k).Value().(type) {
		case string:
			//nolint:errcheck
			fv, _ := strconv.ParseFloat(os.ExpandEnv(v), x64)
			return fv
		default:
			return c.cfg.Get(k).Float()
		}
	}
	//nolint:errcheck
	fv, _ := strconv.ParseFloat(envValue, x64)
	return fv
}

// GetInt extracts k-path value as int,
// values ${val} or $val will be replaced according to the values current environment variables.
func (c *CFG) GetInt(k string) int {
	envValue := c.env(k)
	if envValue == "" {
		switch v := c.cfg.Get(k).Value().(type) {
		case string:
			//nolint:errcheck
			vi, _ := strconv.Atoi(os.ExpandEnv(v))
			return vi
		default:
			return int(c.cfg.Get(k).Int())
		}
	}
	//nolint:errcheck
	vi, _ := strconv.Atoi(os.ExpandEnv(envValue))
	return vi
}

const (
	dec int = 10
)

// GetInt32 extracts k-path value as int32,
// values ${val} or $val will be replaced according to the values current environment variables.
func (c *CFG) GetInt32(k string) int32 {
	envValue := c.env(k)
	if envValue == "" {
		switch v := c.cfg.Get(k).Value().(type) {
		case string:
			//nolint:errcheck
			vi, _ := strconv.ParseInt(os.ExpandEnv(v), dec, x64)
			return int32(vi)
		default:
			return int32(c.cfg.Get(k).Int())
		}
	}
	//nolint:errcheck
	vi, _ := strconv.ParseInt(os.ExpandEnv(envValue), dec, x64)
	return int32(vi)
}

// GetInt64 extracts k-path value as int64,
// values ${val} or $val will be replaced according to the values current environment variables.
func (c *CFG) GetInt64(k string) int64 {
	envValue := c.env(k)
	if envValue == "" {
		switch v := c.cfg.Get(k).Value().(type) {
		case string:
			//nolint:errcheck
			vi, _ := strconv.ParseInt(os.ExpandEnv(v), dec, x64)
			return vi
		default:
			return c.cfg.Get(k).Int()
		}
	}
	//nolint:errcheck
	vi, _ := strconv.ParseInt(os.ExpandEnv(envValue), dec, x64)
	return vi
}

// GetIntSlice extracts k-path value as slice int,
// values ${val} or $val will be replaced according to the values current environment variables.
func (c *CFG) GetIntSlice(k string) []int {
	src := c.cfg.Get(k).Array()
	res := make([]int, len(src))
	for i, x := range src {
		switch v := x.Value().(type) {
		case string:
			//nolint:errcheck
			vi, _ := strconv.Atoi(os.ExpandEnv(v))
			res[i] = vi
		default:
			res[i] = int(x.Int())
		}
	}
	return res
}

//nolint:gocritic
// GetSliceStringAsIs extracts k-path value as slice string,
// values ${val} or $val will be replaced according to the values current environment variables.
func (c *CFG) GetSliceStringAsIs(k string) []string {
	src := c.cfg.Get(k).Array()
	res := make([]string, 0, len(src))
	for _, x := range src {
		switch v := x.Value().(type) {
		case string:
			res = append(res, v)
		}
	}
	return res
}

// GetStringMap extracts k-path value as map[string]interface{},
// values ${val} or $val will be replaced according to the values current environment variables.
//
// ATTENTION: if expected not string values with ${val} or $val replacement, it will be string!
//
// Example:
//
// math:
//   root: ${ROOT_PI} # export ROOT_PI=1.77245, expect 1.77245
// GetStringMap("math") = {"root":"1.77254"}.
func (c *CFG) GetStringMap(k string) map[string]interface{} {
	src := c.cfg.Get(k).Map()
	res := make(map[string]interface{}, len(src))
	for k, x := range src {
		switch v := x.Value().(type) {
		case string:
			res[k] = os.ExpandEnv(v)
		default:
			res[k] = x.Value()
		}
	}
	return res
}

// GetStringMapString extracts k-path value as map[string]string,
// string values ${val} or $val will be replaced according to the values current environment variables.
func (c *CFG) GetStringMapString(k string) map[string]string {
	src := c.cfg.Get(k).Map()
	res := make(map[string]string, len(src))
	for k, v := range src {
		res[k] = os.ExpandEnv(v.String())
	}
	return res
}

// GetSliceMap extracts k-path value as slice map[string]interface{},
// string values ${val} or $val will be replaced according to the values current environment variables.
func (c *CFG) GetSliceMap(k string) []map[string]interface{} {
	src, ok := c.cfg.Get(k).Value().([]interface{})
	if !ok {
		return nil
	}
	res := make([]map[string]interface{}, 0, len(src))
	for _, v := range src {
		if m, mok := v.(map[string]interface{}); mok {
			for km, xm := range m {
				switch vm := xm.(type) {
				case string:
					m[km] = os.ExpandEnv(vm)
				default:
					m[km] = xm
				}
			}
			res = append(res, m)
		}
	}
	return res
}

// helpers.

func (c *CFG) env(k string) string {
	envKey := c.envPrefix + envSeparator + strings.ToUpper(strings.ReplaceAll(k, envParseSeparator, envSeparator))
	if c.envPrefix == "" {
		envKey = strings.ToUpper(strings.ReplaceAll(k, envParseSeparator, envSeparator))
	}
	return os.Getenv(envKey)
}

// helpers

func readConfigFileToMAP(fn string) (map[string]interface{}, error) {
	f, err := os.Open(fn)
	if err != nil {
		return nil, fmt.Errorf("open file error: %w", err)
	}
	defer f.Close()
	data, err := io.ReadAll(f)
	if err != nil {
		return nil, fmt.Errorf("read file error: %w", err)
	}
	_, _, ext := pathSeparator(fn)
	//
	err = formatValidate(ext)
	if err != nil {
		return nil, fmt.Errorf("formatValidate error: %w", err)
	}
	cfg := make(map[string]interface{})
	err = decoderRegistry.Decode(ext, data, &cfg)
	if err != nil {
		return nil, fmt.Errorf("decode file content with format %s error: %w", ext, err)
	}
	return cfg, nil
}

func formatValidate(format string) error {
	for i := range supportedFormats {
		if supportedFormats[i] == format {
			return nil
		}
	}
	return fmt.Errorf("unsupported format %s, allowed only %v", format, supportedFormats)
}

// pathSeparator...
func pathSeparator(in string) (dir, filename, ext string) {
	dir, file := path.Split(in)
	ext = strings.TrimLeft(filepath.Ext(file), ".")
	filename = strings.ReplaceAll(file, "."+ext, "")
	dir = path.Dir(dir)
	return
}
