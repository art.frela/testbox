package config

import (
	"os"
	"reflect"
	"testing"
)

func TestCFG_GetSliceString(t *testing.T) {
	tests := []struct {
		name         string
		pathToConfig string
		envPrefix    string
		pathPrefix   string
		env          map[string]string
		k            string
		want         []string
	}{
		{"yaml_env", "./testdata/yaml/config.yaml", "APP1", "", nil, "environment", []string{"DEBUG=1", "DB=${db:network_address}"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// set envs
			for ek, ev := range tt.env {
				os.Setenv(ek, ev)
			}
			dc, err := New(tt.envPrefix, Schema)
			if err != nil {
				t.Errorf("New() unexpected error %s", err)
				return
			}
			dc.Read(tt.pathToConfig)
			if got := dc.GetSliceStringAsIs(tt.k); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CFG.GetSliceStringAsIs() = %v, want %v", got, tt.want)
			}
			// clean envs
			for ek := range tt.env {
				os.Unsetenv(ek)
			}
		})
	}
}
